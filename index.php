<?php
/**
 * Created by PhpStorm.
 * User: kirya_000
 * Date: 10.05.2017
 * Time: 15:29
 */

public function parse()
    {
        $file = '14931159696454.xml';

        $razd_title = '';       //Названия карточек и разделов
        $group_title = '';
        $podgroup_title = '';
        $vid_title = '';

        $arrGoods = [];         //Выходной массив

        if (file_exists($file)) {
            $xml1 = file_get_contents($file);
            $xml = simplexml_load_string($xml1, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");
            $ns = $xml->getNamespaces(true);
            $soap = $xml->children($ns['SOAP']);
            $res = $soap->Body->children($ns['ns1']);

            //dd($res->children());



            foreach ($res->children() as $item) {
                if ($item->GID == 2145){

                    $this->quickSort($item->AnalyticsItems, 0, count($item[0]->AnalyticsItems)-1);

                    foreach ($item->AnalyticsItems as $aitem){
                        $razd = substr($aitem->GID,0,3);
                        $group = substr($aitem->GID,3,3);
                        $podgroup = substr($aitem->GID,6,2);
                        $vid = substr($aitem->GID,8,2);

                        if ($group=='000'){                             //Для раздела запоминаем его название
                            $razd_title = (string)$aitem->ItemName;
                        }
                        elseif ($podgroup=='000') {
                            $group_title = (string)$aitem->ItemName;    //Для группы запоминаем ее название и записываем код раздела
                            $arrGoods[$razd_title][$group_title]['ItemName'] = (string)$aitem->ItemName;
                            $arrGoods[$razd_title][$group_title]['ShortName'] = (string)$aitem->ShortName;
                            $arrGoods[$razd_title][$group_title]['ProductCode'] = $group;
                            $arrGoods[$razd_title][$group_title]['PreCode'] = $razd;
                        }
                        elseif ($vid=='00'){        //Для подгруппы запоминаем ее название и записываем код группы и данные по условию
                            $podgroup_title = (string)$aitem->ItemName;
                            $arrGoods[$razd_title][$group_title][$podgroup_title]['ItemName'] = (string)$aitem->ItemName;
                            $arrGoods[$razd_title][$group_title][$podgroup_title]['ShortName'] = (string)$aitem->ShortName;
                            $arrGoods[$razd_title][$group_title][$podgroup_title]['ProductCode'] = $podgroup;
                            $arrGoods[$razd_title][$group_title][$podgroup_title]['PreCode'] = $group;
                        }
                        else{
                            $arrGoods[$razd_title][$group_title][$podgroup_title][$vid_title]['ItemName'] = (string)$aitem->ItemName;
                            $arrGoods[$razd_title][$group_title][$podgroup_title][$vid_title]['ShortName'] = (string)$aitem->ShortName;
                            $arrGoods[$razd_title][$group_title][$podgroup_title][$vid_title]['ProductCode'] = $vid;
                            $arrGoods[$razd_title][$group_title][$podgroup_title][$vid_title]['PreCode'] = $podgroup;
                        }
                    }
                }
            }
            return response()->json($arrGoods);

        } else {
            exit('Не удалось открыть файл test.xml.');
        }

    }

    function quickSort(&$arr, $low, $high) {
        $i = $low;
        $j = $high;

        $middle = $arr[intdiv(($low + $high),2)]->GID;
        do {
            while($arr[$i]->GID < $middle) ++$i;  // ищем элементы для правой части
            while($arr[$j]->GID > $middle) -$j;  // ищем элементы для левой части
            if($i <= $j){
                // перебрасываем элементы
                $this->swap($arr[$i], $arr[$j]);

                // следующая итерация
                $i++; $j--;
            }
        }
        while($i < $j);

        if($low < $j){
            // рекурсивно вызываем сортировку для левой части
            $this->quickSort($arr, $low, $j);
        }

        if($i < $high){
            // рекурсивно вызываем сортировку для правой части
            $this->quickSort($arr, $i, $high);
        }
    }

    function swap($first, $second){
        $temp = $first;
        $first = $second;
        $second = $temp;
    }

?>

